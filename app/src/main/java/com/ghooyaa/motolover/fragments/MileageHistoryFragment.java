package com.ghooyaa.motolover.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.AddVehicleActivity;
import com.ghooyaa.motolover.activities.MileageTrackerActivity;
import com.ghooyaa.motolover.activities.ServiceActivity;
import com.ghooyaa.motolover.adapter.MileageHistoryAdapter;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.listeners.IHistoryFilterListener;
import com.ghooyaa.motolover.model.MileageHistory;
import com.ghooyaa.motolover.utils.Utility;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MileageHistoryFragment extends Fragment implements IHistoryFilterListener {

    RecyclerView recyclerView;
    MileageHistoryAdapter adapter;
    ArrayList<MileageHistory> mileageHistory;
    LinearLayoutManager layoutManager;

    FloatingActionButton fab;
    TextView textView;
    ImageView imageView;

    String v_id = "0";
    String vName;
    private String TAG = getClass().getSimpleName();
    private DatabaseHelper mDatabaseHelper = null;

    public MileageHistoryFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mileage, container, false);

        recyclerView = view.findViewById(R.id.mileage_recycler);
        mileageHistory = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,true);
        layoutManager.setInitialPrefetchItemCount(0);
        recyclerView.setLayoutManager(layoutManager);

        textView = view.findViewById(R.id.text_mileage);
        imageView = view.findViewById(R.id.image_mileage);

        fab = view.findViewById(R.id.fab_mileage);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.equals(vName,null))
                {
                    Utility.alertDialogShow(getActivity(),"Alert","Please select a vehicle");
                }
                else
                {
                    Intent intent = new Intent(getActivity(), MileageTrackerActivity.class);
                    intent.putExtra("id",v_id);
                    intent.putExtra("name",vName);
                    startActivityForResult(intent,2);
                }
            }
        });

        mDatabaseHelper = new DatabaseHelper(getContext());
        setAdapter(filterVehicle(v_id));
        return view;
    }


    private void setAdapter(ArrayList<MileageHistory> mileageHistoryList) {
        if (mileageHistoryList != null) {
            Log.d(TAG, "mileageHistoryList : " + mileageHistoryList.size());
            adapter = new MileageHistoryAdapter(mileageHistoryList, getActivity());
            recyclerView.setAdapter(adapter);
            if (mileageHistoryList.size()==0)
            {
                textView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
            }
            else
            {
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }
            adapter.notifyDataSetChanged();
        }

    }

    private ArrayList<MileageHistory> filterVehicle(String vehicleId) {
        if (mDatabaseHelper != null) {
            Log.d(TAG,"mileageHistories in not null & id is :  "+vehicleId);
            return mDatabaseHelper.getMileage(vehicleId);
        }
        return null;
    }


  /*  @Override
    public void onRefreshFilter(String vehicleId) {

        Log.d(TAG,"onRefreshFilter : "+vehicleId);
        ArrayList<MileageHistory>  mileageHistories = filterVehicle(vehicleId);
        if(mileageHistories!=null) {
            setAdapter(mileageHistories);
        }//setAdapter(filterVehicle(vehicleId));
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode!=0 && data != null)
        {
            if(requestCode == 2)

                setAdapter(filterVehicle(data.getStringExtra("v_id")));
        }
    }

    @Override
    public void onRefreshFilter(String vehicleId, String vehicleName) {

        v_id = vehicleId;
        vName = vehicleName;

        Log.d(TAG,"onRefreshFilter : "+vehicleId);
        ArrayList<MileageHistory>  mileageHistories = filterVehicle(vehicleId);
        if(mileageHistories!=null) {
            Log.d(TAG,"mileageHistories in not null ");
            setAdapter(mileageHistories);
        }

    }
}
