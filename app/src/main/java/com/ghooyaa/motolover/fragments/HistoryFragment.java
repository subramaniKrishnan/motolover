package com.ghooyaa.motolover.fragments;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.MainActivity;
import com.ghooyaa.motolover.activities.MileageTrackerActivity;
import com.ghooyaa.motolover.adapter.MileageHistoryAdapter;
import com.ghooyaa.motolover.adapter.TabAdapter;
import com.ghooyaa.motolover.custom.ListviewDialog;
import com.ghooyaa.motolover.model.MileageHistory;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;
    String v_id = "0";
    String vehicle_name;
    TabAdapter mTabAdapter;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    private EditText editText_toolbar;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        /*Bundle bundle = getActivity().getIntent().getExtras();
        String Name = bundle.getString("id");
        Toast.makeText(getContext(), "name : " + Name, Toast.LENGTH_SHORT).show();*/

        View view = inflater.inflate(R.layout.fragment_history, container, false);

        //toolbar = view.findViewById(R.id.toolbar_history);
        //editText_toolbar = view.findViewById(R.id.edit_history);
        tabLayout = view.findViewById(R.id.tablayout);
        viewPager = view.findViewById(R.id.pager);

        //sharedPreferences = getActivity().getSharedPreferences("key",getContext().MODE_PRIVATE);

        ActionBar mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.res_edittext);
        View view1 =((AppCompatActivity) getActivity()).getSupportActionBar().getCustomView();

        final EditText editText = view1.findViewById(R.id.edittext_history);
        //editText.setWidth(500);
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ListviewDialog listviewDialog = new ListviewDialog(getContext(), new ListviewDialog.iDialog() {
                    @Override
                    public void setVehicle(String str, String vid) {
                        editText.setText(str);
                        v_id = vid;

                       // Log.d("HistoryFragment", "viewPager.getCurrentItem() : " + viewPager.getCurrentItem());
                       // mTabAdapter.doRefresh(v_id);

                        mTabAdapter.doRefresh(v_id,editText.getText().toString());
                        Log.d("HistoryFragment", "History fragment : " + v_id);

                        /*vehicle_name = editText.getText().toString();
                        editor = sharedPreferences.edit();
                        editor.putString("key",vehicle_name);
                        editor.putString("id",v_id);
                        editor.commit();*/

                    }
                }, v_id);
                listviewDialog.show();

            }
        });




        /*if(sharedPreferences.contains("key"))
        {
            editText.setText(sharedPreferences.getString("key",""));
        }
        if (sharedPreferences.contains("id"))
        {
            mTabAdapter.doRefresh(viewPager.getCurrentItem(), sharedPreferences.getString("id",""));
        }*/

        /*editText_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListviewDialog listviewDialog = new ListviewDialog(getContext(), new ListviewDialog.iDialog() {
                    @Override
                    public void setVehicle(String str, String vid) {
                        editText_toolbar.setText(str);
                        v_id = vid;
                        mTabAdapter.doRefresh(viewPager.getCurrentItem(), v_id);
                        Log.d("tabadapter", "History fragment : " + v_id);
                    }
                }, v_id);
                listviewDialog.show();
            }
        });*/

        mTabAdapter = new TabAdapter(getChildFragmentManager());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(mTabAdapter);

        return view;
    }

    /*@Override
    public void passData(String name, String id) {

        if(name != null && id != null) {
            Toast.makeText(getContext(), "name : " + name + "\n" + "id : " + id, Toast.LENGTH_SHORT).show();
        }
    }*/
}
