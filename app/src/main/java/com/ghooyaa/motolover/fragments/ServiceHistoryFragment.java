package com.ghooyaa.motolover.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.AddVehicleActivity;
import com.ghooyaa.motolover.activities.ServiceActivity;
import com.ghooyaa.motolover.adapter.ServiceHistoryAdapter;
import com.ghooyaa.motolover.adapter.VehicleListAdapter;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.listeners.IHistoryFilterListener;
import com.ghooyaa.motolover.model.MileageHistory;
import com.ghooyaa.motolover.model.ServiceHistory;
import com.ghooyaa.motolover.model.VehicleList;
import com.ghooyaa.motolover.utils.Utility;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceHistoryFragment extends Fragment implements IHistoryFilterListener {

    RecyclerView recyclerView;
    ServiceHistoryAdapter adapter;
    ArrayList<ServiceHistory> serviceList;
    LinearLayoutManager layoutManager;

    FloatingActionButton fab;
    TextView textView;
    ImageView imageView;

    View view1;

    String vid;
    String vName = null;

    DatabaseHelper mDatabaseHelper = null;

    public ServiceHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);

        view1 = view;

        recyclerView = view.findViewById(R.id.service_recycler);
        serviceList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(layoutManager);

        textView = view.findViewById(R.id.text_service);
        imageView = view.findViewById(R.id.image_service);


        fab = view.findViewById(R.id.fab_service);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.equals(vName,null))
                {
                    Utility.alertDialogShow(getActivity(),"Alert","Please select a vehicle");
                }
                else
                {
                    Intent intent = new Intent(getActivity(), ServiceActivity.class);
                    intent.putExtra("id",vid);
                    intent.putExtra("name",vName);
                    startActivityForResult(intent,3);
                }

            }
        });

        mDatabaseHelper = new DatabaseHelper(getContext());
        setAdapter(filterService(vid));

        return view;
    }

    public void setAdapter(ArrayList<ServiceHistory> serviceHistory) {

        adapter = new ServiceHistoryAdapter(serviceHistory, getContext());
        recyclerView.setAdapter(adapter);
        if (serviceHistory.size()==0)
        {
            textView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
        }
        else
        {
            textView.setVisibility(View.GONE);
            imageView.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }

    private ArrayList<ServiceHistory> filterService(String vid) {
        if (mDatabaseHelper != null) {

            return mDatabaseHelper.getService(vid);
        }
        return null;
    }

    /*@Override
    public void onRefreshFilter(String vehicleId) {

        ArrayList<ServiceHistory> serviceHistories =  filterService(vehicleId);
        if (serviceHistories != null) {
            setAdapter(serviceHistories);
        }
    }*/

    @Override
    public void onRefreshFilter(String vehicleId, String vehicleName) {

        vName = vehicleName;
        vid = vehicleId;

        ArrayList<ServiceHistory> serviceHistories = filterService(vehicleId);
        if (serviceHistories != null) {
            setAdapter(serviceHistories);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode != 0 && data != null) {
            if (requestCode == 3) {
                setAdapter(filterService(data.getStringExtra("v_id")));
            }
        }

    }


}
