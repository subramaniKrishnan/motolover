package com.ghooyaa.motolover.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.adapter.CostAdapter;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.model.CostHistory;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CostFragment extends Fragment {

    RecyclerView recyclerView= null;
    LinearLayoutManager manager = null;
    ArrayList<CostHistory> costHistories = null;
    CostAdapter adapter = null;

    DatabaseHelper mDatabaseHelper = null;


    public CostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cost, container, false);

        ActionBar mActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setCustomView(R.layout.res_edittext);
        View view1 =((AppCompatActivity) getActivity()).getSupportActionBar().getCustomView();

        final EditText editText = view1.findViewById(R.id.edittext_history);

        mDatabaseHelper = new DatabaseHelper(getContext());
        recyclerView = view.findViewById(R.id.cost_recyclerview);
        costHistories = new ArrayList<>();
        manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,true);
        recyclerView.setLayoutManager(manager);

        costHistories = mDatabaseHelper.getCost();


        adapter = new CostAdapter(getContext(),costHistories);
        recyclerView.setAdapter(adapter);



        return view;
    }

}
