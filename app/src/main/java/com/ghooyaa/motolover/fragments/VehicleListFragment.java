package com.ghooyaa.motolover.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.AddVehicleActivity;
import com.ghooyaa.motolover.activities.ManageActivity;
import com.ghooyaa.motolover.adapter.ServiceHistoryAdapter;
import com.ghooyaa.motolover.adapter.VehicleListAdapter;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.model.ServiceHistory;
import com.ghooyaa.motolover.model.VehicleList;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehicleListFragment extends Fragment implements VehicleListAdapter.onResult {

    RecyclerView recyclerView;
    VehicleListAdapter adapter;
    ArrayList<VehicleList> vehicleList;
    LinearLayoutManager layoutManager;

    DatabaseHelper mDatabaseHelper = null;

    FloatingActionButton fab;
    TextView textView;
    ImageView imageView;

    public VehicleListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //pull test
        View view = inflater.inflate(R.layout.fragment_vehiclelist, container, false);

        ActionBar mActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();

        mActionBar.setDisplayShowTitleEnabled(true);
        mActionBar.setDisplayShowCustomEnabled(false);

        recyclerView = view.findViewById(R.id.vehicleList_recycler);
        vehicleList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        recyclerView.setLayoutManager(layoutManager);

        textView = view.findViewById(R.id.text_vehicleList);
        imageView = view.findViewById(R.id.image_vehicleList);

        fab = view.findViewById(R.id.fab_vehiclelist);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(getActivity(), AddVehicleActivity.class);
                startActivityForResult(intent, 1);*/
                startActivity();
            }
        });

        mDatabaseHelper = new DatabaseHelper(getContext());
        setAdapter(filterService());

        return view;
    }

        public void setAdapter(ArrayList< VehicleList > vehicleList) {
        adapter = new VehicleListAdapter(vehicleList, getContext(),this);
        recyclerView.setAdapter(adapter);
            if (vehicleList.size()==0)
            {
                textView.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.VISIBLE);
            }
            else
            {
                textView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }
        adapter.notifyDataSetChanged();
    }

        private ArrayList<VehicleList> filterService() {
        if (mDatabaseHelper != null) {

            return mDatabaseHelper.getVehicleList();
        }
        return null;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("VehicleListFragment","requestCode : "+requestCode);

        if(resultCode!=0 && data != null )
        {
            if(requestCode==1)
            {
                Log.d("VehicleListFragment","requestCode : "+requestCode);
                setAdapter(filterService());
            }
        }
    }

    public void startActivity(){

        Intent intent = new Intent(getActivity(), AddVehicleActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityRes(String id) {

        Toast.makeText(getContext(), "onActivityRes", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), ManageActivity.class);
        intent.putExtra("ID",id);
        startActivityForResult(intent,1);

    }
}




























