package com.ghooyaa.motolover.model;

/**
 * Created by nekhop on 8/8/2017.
 */
public class ServiceHistory {

    String service_last;
    String service_next;
    String service_odometer;
    String service_amount;

    public String getService_last() {
        return service_last;
    }

    public void setService_last(String service_last) {
        this.service_last = service_last;
    }

    public String getService_next() {
        return service_next;
    }

    public void setService_next(String service_next) {
        this.service_next = service_next;
    }

    public String getService_odometer() {
        return service_odometer;
    }

    public void setService_odometer(String service_odometer) {
        this.service_odometer = service_odometer;
    }

    public String getService_amount() {
        return service_amount;
    }

    public void setService_amount(String service_amount) {
        this.service_amount = service_amount;
    }
}
