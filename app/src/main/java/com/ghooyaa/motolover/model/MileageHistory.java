package com.ghooyaa.motolover.model;

/**
 * Created by nekhop on 8/8/2017.
 */
public class MileageHistory {

    String mileage;
    String distance;
    String cost;
    String no_of_days;

    String mileage_unit;
    String distance_unit;
    String cost_unit;

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getNo_of_days() {
        return no_of_days;
    }

    public void setNo_of_days(String no_of_days) {
        this.no_of_days = no_of_days;
    }

    public String getMileage_unit() {
        return mileage_unit;
    }

    public void setMileage_unit(String mileage_unit) {
        this.mileage_unit = mileage_unit;
    }

    public String getDistance_unit() {
        return distance_unit;
    }

    public void setDistance_unit(String distance_unit) {
        this.distance_unit = distance_unit;
    }

    public String getCost_unit() {
        return cost_unit;
    }

    public void setCost_unit(String cost_unit) {
        this.cost_unit = cost_unit;
    }
}
