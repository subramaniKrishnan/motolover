package com.ghooyaa.motolover.model;

/**
 * Created by nekhop on 9/2/2017.
 */
public class CostHistory {

    String month;
    String no_of_times_filled;
    String total_amount;
    String currency;


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getNo_of_times_filled() {
        return no_of_times_filled;
    }

    public void setNo_of_times_filled(String no_of_times_filled) {
        this.no_of_times_filled = no_of_times_filled;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
