package com.ghooyaa.motolover.model;

/**
 * Created by nekhop on 8/3/2017.
 */
public class ListItems {

    String vehicle_image;
    String vehicle_name;
    String vehicle_Rno;
    int check_image;
    String vehicle_id;

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }

    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_Rno() {
        return vehicle_Rno;
    }

    public void setVehicle_Rno(String vehicle_Rno) {
        this.vehicle_Rno = vehicle_Rno;
    }

    public int getCheck_image() {
        return check_image;
    }

    public void setCheck_image(int check_image) {
        this.check_image = check_image;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }
}
