package com.ghooyaa.motolover.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.model.CostHistory;
import com.ghooyaa.motolover.model.ListItems;
import com.ghooyaa.motolover.model.MileageHistory;
import com.ghooyaa.motolover.model.ServiceHistory;
import com.ghooyaa.motolover.model.VehicleList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nekhop on 8/2/2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    String TAG = "DatabaseHelper";

    final public static String DATABASE_NAME = "Motolover_database";
    final String TABLE_VEHICLE = "vehicle_table";
    final String TABLE_SERVICE = "service_table";
    final String TABLE_FILL = "fill_table";
    final String TABLE_REFILL = "refill_table";
    final String TABLE_MILEAGE = "mileage_table";


    final String VEHICLE_ID = "id";
    final String VEHICLE_NAME = "vehicle_name";
    final String VEHICLE_NO = "vehicle_no";
    final String VEHICLE_IMAGE = "vehicle_type";
    final String DISTANCE_UNIT = "distance_unit";
    final String FUEL_UNIT = "fuel_unit";
    final String CONSUMPTION_UNIT = "consumption_unit";
    final String LAST_SERVICE_DATE = "last_service_date";
    final String LAST_SERVICE_COST = "last_service_cost";
    final String LAST_SERVICE_READING = "last_service_reading";
    final String NEXT_SERVICE_DATE = "next_service_date";
    final String FILL_READING = "fill_reading";
    final String FILL_FUEL_PRICE = "fill_fuel_price";
    final String FILL_AMOUNT = "fill_amount";
    final String FILL_FUEL_QUANTITY = "fill_fuel_quantity";
    final String FILL_DATE = "fill_date";
    final String REFILL_READING = "refill_reading";
    final String REFILL_FUEL_PRICE = "refill_fuel_price";
    final String REFILL_AMOUNT = "refill_amount";
    final String REFILL_FUEL_QUANTITY = "refill_fuel_quantity";
    final String REFILL_DATE = "refill_date";
    final String MILEAGE = "mileage";
    final String DISTANCE = "distance";
    final String COST = "cost";
    final String NO_OF_DAYS = "no_of_days";

    final String VEHICLE_SERVICE = "vehicle_service";
    final String VEHICLE_FILL = "vehicle_fill";
    final String VEHICLE_REFILL = "vehicle_refill";
    final String VEHICLE_MILEAGE = "vehicle_mileage";

    final String MILEAGE_UNIT = "mileage_unit";
    final String sDISTANCE_UNIT = "S_distance_unit";
    final String COST_UNIT = "cost_unit";

    final String IMAGE_TABLE = "image_table";
    final String IMAGE = "vehicle_image";

    String VEHICLE_QUERY = "CREATE TABLE " + TABLE_VEHICLE + "(id INTEGER PRIMARY KEY AUTOINCREMENT, vehicle_name TEXT, vehicle_no TEXT, vehicle_type TEXT," +
            "distance_unit TEXT, fuel_unit TEXT, consumption_unit TEXT, last_service_date TEXT,next_service_date TEXT)";

    String SERVICE_QUERY = "CREATE TABLE " + TABLE_SERVICE + "(vehicle_service TEXT, last_service_date TEXT, last_service_cost TEXT, last_service_reading TEXT, next_service_date TEXT)";

    String FILL_QUERY = "CREATE TABLE " + TABLE_FILL + "(vehicle_fill TEXT, fill_reading TEXT, fill_fuel_price TEXT, fill_amount TEXT, fill_fuel_quantity TEXT, fill_date TEXT)";

    String REFILL_QUERY = "CREATE TABLE " + TABLE_REFILL + "(vehicle_refill TEXT, refill_reading TEXT, refill_fuel_price TEXT, refill_amount TEXT, refill_fuel_quantity TEXT, refill_date TEXT)";

    String MILEAGE_QUERY = "CREATE TABLE " + TABLE_MILEAGE + "(vehicle_mileage TEXT, mileage TEXT, distance TEXT, cost TEXT, no_of_days TEXT, mileage_unit TEXT, S_distance_unit TEXT, cost_unit TEXT)";

    String IMAGE_QUERY = "CREATE TABLE " + IMAGE_TABLE + "(vehicle_image BLOB)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 5);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(VEHICLE_QUERY);
        sqLiteDatabase.execSQL(SERVICE_QUERY);
        sqLiteDatabase.execSQL(FILL_QUERY);
        sqLiteDatabase.execSQL(REFILL_QUERY);
        sqLiteDatabase.execSQL(MILEAGE_QUERY);
        sqLiteDatabase.execSQL(IMAGE_QUERY);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean insertVehicle(String v_name, String v_no, String v_type, String d_unit, String f_unit, String c_unit, String last_date/*, String next_date*/) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_NAME, v_name);
        values.put(VEHICLE_NO, v_no);
        values.put(VEHICLE_IMAGE, v_type);
        values.put(DISTANCE_UNIT, d_unit);
        values.put(FUEL_UNIT, f_unit);
        values.put(CONSUMPTION_UNIT, c_unit);
        values.put(LAST_SERVICE_DATE, last_date);
        // values.put(NEXT_SERVICE_DATE,next_date);

        long result = database.insert(TABLE_VEHICLE, null, values);
        return true;
    }

    public boolean insertService(String R_no, String last_service_date, String last_service_cost, String last_service_reading, String next_service_date) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_SERVICE, R_no);
        values.put(LAST_SERVICE_DATE, last_service_date);
        values.put(LAST_SERVICE_COST, last_service_cost);
        values.put(LAST_SERVICE_READING, last_service_reading);
        values.put(NEXT_SERVICE_DATE, next_service_date);

        long result = database.insert(TABLE_SERVICE, null, values);
        return true;
    }


    public boolean insertFill(String R_no, String last_reading, String fuel_price, String amount, String fuel_quantity, String fill_date) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_FILL, R_no);
        values.put(FILL_READING, last_reading);
        values.put(FILL_FUEL_PRICE, fuel_price);
        values.put(FILL_AMOUNT, amount);
        values.put(FILL_FUEL_QUANTITY, fuel_quantity);
        values.put(FILL_DATE, fill_date);

        long result = database.insert(TABLE_FILL, null, values);
        return true;
    }

    public boolean insertReFill(String R_no, String current_reading, String fuel_price, String amount, String fuel_quantity, String refill_date) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_REFILL, R_no);
        values.put(REFILL_READING, current_reading);
        values.put(REFILL_FUEL_PRICE, fuel_price);
        values.put(REFILL_AMOUNT, amount);
        values.put(REFILL_FUEL_QUANTITY, fuel_quantity);
        values.put(REFILL_DATE, refill_date);

        long result = database.insert(TABLE_REFILL, null, values);
        return true;
    }


    public boolean insertMileage(String R_no, String mileage, int distance, String cost, long no_of_days, String mileage_unit, String distance_unit, String cost_unit) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_MILEAGE, R_no);
        values.put(MILEAGE, mileage);
        values.put(DISTANCE, distance);
        values.put(COST, cost);
        values.put(NO_OF_DAYS, no_of_days);
        values.put(MILEAGE_UNIT, mileage_unit);
        values.put(sDISTANCE_UNIT, distance_unit);
        values.put(COST_UNIT, cost_unit);

        long result = database.insert(TABLE_MILEAGE, null, values);
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getExactVehicle(String V_id) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "select * from vehicle_table where id='" + V_id + "'";
        Cursor cr = sqLiteDatabase.rawQuery(query, null);
        return cr;
    }

    public Cursor getLastReFill(String V_id) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_REFILL + " WHERE vehicle_refill='" + V_id + "'";
        Cursor cr = sqLiteDatabase.rawQuery(query, null);

        return cr;
    }

    public Cursor getLastFill(String V_id) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_FILL + " WHERE vehicle_fill='" + V_id + "'";
        Cursor cr = sqLiteDatabase.rawQuery(query, null);

        return cr;
    }

    public Cursor getLastService(String vehicleId) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SERVICE + " WHERE vehicle_service='" + vehicleId + "'";
        Cursor CR = sqLiteDatabase.rawQuery(query, null);
        return CR;
    }

    public ArrayList<ListItems> getVehicles() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<ListItems> list = new ArrayList<ListItems>();
        String v_type;

        String query = "SELECT * FROM " + TABLE_VEHICLE;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                ListItems listItems = new ListItems();
                listItems.setVehicle_name(cursor.getString(1));
                listItems.setVehicle_Rno(cursor.getString(2));
                listItems.setCheck_image(R.drawable.check_icon);
                v_type = cursor.getString(3);
                /*if(v_type.equals("bike"))
                {
                    listItems.setVehicle_image(R.drawable.bike);
                }
                else
                {
                    listItems.setVehicle_image(R.drawable.car);
                }*/

                list.add(listItems);
            }
        }
        return list;
    }

    public ArrayList<ListItems> getSelectedVehicle(String vid) {


        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<ListItems> list = new ArrayList<ListItems>();
        String v_type;

        String query = "SELECT * FROM " + TABLE_VEHICLE;
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                ListItems listItems = new ListItems();
                listItems.setVehicle_id(cursor.getString(0));
                listItems.setVehicle_name(cursor.getString(1));
                listItems.setVehicle_Rno(cursor.getString(2));
                listItems.setCheck_image(R.drawable.check_icon);
                listItems.setVehicle_image(cursor.getString(3));
                Log.d("Database", "imageURI in getSelectedVehicle : " + cursor.getString(3));
                /*v_type = cursor.getString(3);
                if(v_type.equals("bike"))
                {
                    listItems.setVehicle_image(R.drawable.bike);
                }
                else
                {
                    listItems.setVehicle_image(R.drawable.car);
                }*/

                Log.d("DB", "vid = " + cursor.getString(0) + "selected id = " + vid);
                if (cursor.getString(0).equals(vid)) {
                    listItems.setCheck_image(1);
                } else {
                    listItems.setCheck_image(0);
                }
                list.add(listItems);
            }
        }
        return list;

    }

    public ArrayList<VehicleList> getVehicleList() {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ArrayList<VehicleList> vehicleList = new ArrayList<>();
        String v_type;

        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_VEHICLE, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                VehicleList listItems = new VehicleList();
                listItems.setVehicle_id(cursor.getString(0));
                listItems.setVehicle_name(cursor.getString(1));
                listItems.setVehicle_no(cursor.getString(2));
                listItems.setService_date(cursor.getString(7));
                listItems.setVehicle_image(cursor.getString(3));
                Log.d("Database", "imageURI is : " + cursor.getString(3));
                // v_type = cursor.getString(3);
                /*if(v_type.equals("bike"))
                {
                    listItems.setVehicle_image(R.drawable.bike);
                }
                else
                {
                    listItems.setVehicle_image(R.drawable.car);
                }*/
                vehicleList.add(listItems);
            }
        }
        return vehicleList;
    }

    public ArrayList<ServiceHistory> getService(String vid) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ArrayList<ServiceHistory> serviceList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_SERVICE + " WHERE vehicle_service='" + vid + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                ServiceHistory listItems = new ServiceHistory();
                listItems.setService_last(cursor.getString(1));
                listItems.setService_amount(cursor.getString(2));
                listItems.setService_odometer(cursor.getString(3));
                serviceList.add(listItems);
            }
        }
        return serviceList;
    }

    public ArrayList<MileageHistory> getMileage(String V_id) {
        Log.d("DatabaseHelper", "Vid is : " + V_id);
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<MileageHistory> mileageList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_MILEAGE + " WHERE vehicle_mileage='" + V_id + "'";

        //Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_MILEAGE,null);
        Cursor cursor = db.rawQuery(query, null);

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                MileageHistory listItems = new MileageHistory();
                listItems.setMileage(cursor.getString(1));
                listItems.setDistance(cursor.getString(2));
                listItems.setCost(cursor.getString(3));
                listItems.setNo_of_days(cursor.getString(4));

                listItems.setMileage_unit(cursor.getString(5) + "/" + cursor.getString(6));
                listItems.setDistance_unit(cursor.getString(5) + "s");
                listItems.setCost_unit(cursor.getString(7));

                mileageList.add(listItems);

                Log.d("tabadapter", "from db : " + cursor.getString(1) + "\n" + cursor.getString(2) + "\n" + cursor.getString(3) + "\n" + cursor.getString(4));
            }
        }
        return mileageList;
    }

    public void deleteVehicle(String vehicleID) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_VEHICLE, "id=?", new String[]{vehicleID});
        //db.delete(TBL_NAME, "Google=?", new String[]{Integer.toString(id)});

    }

    public void deleteFill(String vehicleID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FILL, "vehicle_fill=?", new String[]{vehicleID});
    }

    public void deleteReFill(String vehicleID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REFILL, "vehicle_refill=?", new String[]{vehicleID});
    }

    public void deleteService(String vehicleID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SERVICE, "vehicle_service=?", new String[]{vehicleID});
    }

    public void deleteMileage(String vehicleID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MILEAGE, "vehicle_mileage=?", new String[]{vehicleID});
    }

    public boolean updateVehicle(String vehicle_id, String name, String no, String v_type, String distance, String fuel, String consumption, String last_date) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VEHICLE_NAME, name);
        values.put(VEHICLE_NO, no);
        values.put(VEHICLE_IMAGE, v_type);
        values.put(DISTANCE_UNIT, distance);
        values.put(FUEL_UNIT, fuel);
        values.put(CONSUMPTION_UNIT, consumption);
        values.put(LAST_SERVICE_DATE, last_date);
        //values.put(NEXT_SERVICE_DATE,next_date);

        long result = db.update(TABLE_VEHICLE, values, "id=" + vehicle_id, null);
        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean updateMileage(String v_id, String distance, String fuel, String cost) {

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(MILEAGE_UNIT, distance);
        values.put(sDISTANCE_UNIT, fuel);
        values.put(COST_UNIT, cost);

        long result = database.update(TABLE_MILEAGE, values, "vehicle_mileage=" + v_id, null);
        if (result == -1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean insertImage(byte[] imageInByte) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IMAGE, imageInByte);
        long result = db.insert(IMAGE_TABLE, null, cv);
        if (result == -1) {
            return false;
        } else {
            return true;
        }


    }

    public ArrayList<CostHistory> getCost() {

        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<CostHistory> costHistory = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_FILL, null);
        Cursor cursor1 = db.rawQuery("SELECT * FROM " + TABLE_REFILL, null);

        float cost = 0;
        float cost1 = 0;
        Date date = null;
        Date date1 = null;
        String month = null;
        String month1 = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Map<String,ArrayList<String>> map = new HashMap<>();




        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (cost == 0) {
                    cost = Float.parseFloat(cursor.getString(3));
                    try {
                        date = dateFormat.parse(cursor.getString(5));
                        month1 = (String) android.text.format.DateFormat.format("MMMM", date);
                       // Log.d(TAG, "Date : " + date);

                       // Log.d("CustomArray","Update Key "+month1+"  Cost : "+cursor.getString(3));

                        if(map.containsValue(month1))
                        {
                         //   Log.d("CustomArray","Update Key "+month1+"  Cost : "+cursor.getString(3));

                            ArrayList<String> temp = map.get(month1);
                            temp.add(cursor.getString(3));
                            map.put(month1,temp);


                        }else{

                            ArrayList<String> temp = new ArrayList<>();
                            temp.add(cursor.getString(3));
                            map.put(month1,temp);

                         //   Log.d("CustomArray","New Key "+month1+"  Cost : "+cursor.getString(3));

                        }

                        // Log.d("CustomArray",map.toString());

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


        //start


        Map<String,ArrayList<String>> map2 = new HashMap<>();

        if (cursor1.getCount() > 0) {
            while (cursor1.moveToNext()) {

                CostHistory listItems = new CostHistory();
                //datecheck(cost,cost1,date1,cursor1,month);
                if (cost != 0 && cost1 == 0) {
                    cost1 = cost + cost1 + Float.parseFloat(cursor1.getString(3));
                    try {
                        date1 = dateFormat.parse(cursor1.getString(5));
                        Log.d(TAG, "Date1 : " + date1);
                        month = (String) android.text.format.DateFormat.format("MMMM", date1);
                        Log.d(TAG, "month : " + month);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (cost1 != 0) {
                    cost1 = cost1 + Float.parseFloat(cursor1.getString(3));
                    try {
                        date1 = dateFormat.parse(cursor1.getString(5));
                        Log.d(TAG, "Date1 : " + date1);
                        month = (String) android.text.format.DateFormat.format("MMMM", date1);
                        Log.d(TAG, "month : " + month);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }


                if(map2.containsKey(month))
                {
                    Log.d("CustomArray","Update Key "+month+"  Cost : "+cursor1.getString(3));

                    ArrayList<String> temp = map2.get(month);

                    temp.add(cursor1.getString(3));
                    map2.put(month,temp);


                }else{

                    ArrayList<String> temps = new ArrayList<>();
                    temps.add(cursor1.getString(3));
                    map2.put(month,temps);

                    Log.d("CustomArray","New Key "+month+"  Cost : "+cursor1.getString(3));

                }

                Log.d("CustomArray2",map2.toString());

                Log.d(TAG, "cost : " + cost1);
                if (!(TextUtils.equals(month, null))) {

                    if ((TextUtils.equals(month1, month))) {
                        Log.d(TAG, "month 1 : " + month1);
                        Log.d(TAG, "month 2 : " + month);
                        listItems.setTotal_amount(String.valueOf(cost1));
                        listItems.setMonth(month1);
                        costHistory.add(listItems);
                        cost1=0;

                    } else {
                        Log.d(TAG, "month 3 : " + month1);
                        Log.d(TAG, "month 4 : " + month);
                        listItems.setTotal_amount(String.valueOf(cost1));
                        listItems.setMonth(month);
                        costHistory.add(listItems);
                        cost1=0;
                    }
                }
            }
        }

        return costHistory;
    }

    private void datecheck(float cost, float cost1, Date date1, Cursor cursor1, String month) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (cost != 0 && cost1 == 0) {
            cost1 = cost + cost1 + Float.parseFloat(cursor1.getString(3));
            try {
                date1 = dateFormat.parse(cursor1.getString(5));
                Log.d(TAG, "Date1 : " + date1);
                month = (String) android.text.format.DateFormat.format("MMMM", date1);
                Log.d(TAG, "month : " + month);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (cost1 != 0) {
            cost1 = cost1 + Float.parseFloat(cursor1.getString(3));
            try {
                date1 = dateFormat.parse(cursor1.getString(5));
                Log.d(TAG, "Date1 : " + date1);
                month = (String) android.text.format.DateFormat.format("MMMM", date1);
                Log.d(TAG, "month : " + month);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }


}
