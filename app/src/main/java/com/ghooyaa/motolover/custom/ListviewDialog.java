package com.ghooyaa.motolover.custom;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.MileageTrackerActivity;
import com.ghooyaa.motolover.adapter.ListViewAdapter;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.model.ListItems;

import java.util.ArrayList;

/**
 * Created by nekhop on 8/4/2017.
 */
public class ListviewDialog extends Dialog {

    TextView name;
    TextView R_no;
    ImageView check_icon;
    TextView V_id;
    String result;
    String vehicle_id;

    public iDialog dialog;

    public interface iDialog{

        void setVehicle(String str,String vid);

    }

    public ListviewDialog(Context context, iDialog dialog,String vid) {
        super(context);
        this.vehicle_id=vid;
        this.dialog = dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.listview_vehicles);

        ListView listView = findViewById(R.id.listview);
        ArrayList<ListItems> item;
        ListViewAdapter listViewAdapter;
        DatabaseHelper database1 = new DatabaseHelper(getContext());
        item = database1.getSelectedVehicle(this.vehicle_id);
        Log.d("current_item_value",""+this.vehicle_id);
        listViewAdapter = new ListViewAdapter(item,getContext());
        listView.setAdapter(listViewAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               dismiss();
               name = view.findViewById(R.id.list_name);
               R_no = view.findViewById(R.id.list_Rno);
               V_id = view.findViewById(R.id.list_id);
               result = name.getText().toString()+"-"+R_no.getText().toString();
                Log.d("ListviewDialog","onItemClick : "+V_id.getText().toString());

               dialog.setVehicle(result,V_id.getText().toString());

            }
        });







    }
}

