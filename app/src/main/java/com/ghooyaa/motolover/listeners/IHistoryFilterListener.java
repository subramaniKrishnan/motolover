package com.ghooyaa.motolover.listeners;

/**
 * Created by Moorthy on 8/13/2017.
 */

public interface IHistoryFilterListener {

    //void onRefreshFilter(String vehicleId);
    void onRefreshFilter(String vehicleId, String vehicleName);
}
