package com.ghooyaa.motolover.utils;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.DatePicker;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by nekhop on 8/8/2017.
 */
public class Utility {

    public static AlertDialog.Builder alertDialogShow(Context context, String title , String message)
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        //final AlertDialog dialog = alertDialog.create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.show();
        return alertDialog;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public long dateFormat(String date1, String date2){

        Log.d("Utility","Fill data : "+date1+"\n"+"refill date : "+date2);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        long diff = 0;
        long no_of_days;

        Date fillDate = null;
        Date refillDate = null;

        try {
            fillDate = sdf.parse(date1);
            Log.d("Utility","Fill data 1 : "+fillDate);
        } catch (ParseException e) {
            Log.d("Utility","Fill data 1 : "+fillDate);
            e.printStackTrace();
        }
        try {
            refillDate = sdf.parse(date2);
            Log.d("Utility","Fill data 2 : "+refillDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (refillDate == null ) {
            diff = 0;
        }
        else {
            diff = refillDate.getTime() - fillDate.getTime();
        }

        if(!(diff<=0))
        {
            no_of_days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        }
        else
        {
            no_of_days = -1;
        }

        return no_of_days;
    }



}
