package com.ghooyaa.motolover.utils;

/**
 * Created by subbu on 8/20/2017.
 */

public class Log {

    private static final String TAG = Log.class.getSimpleName();


    public static void v(String tag, String msg) {

           android.util.Log.v(tag,msg);
    }

    public static void v(String tag, String msg,Throwable th) {

           android.util.Log.v(tag,msg,th);
    }


    public static void d(String tag, String msg) {

           android.util.Log.d(tag,msg);
    }
    public static void d(String tag, String msg, Throwable th) {

        android.util.Log.d(tag,msg,th);

    }

    public static void i(String tag, String msg) {
        android.util.Log.i(tag,msg);

    }

    public static void i(String tag, String msg, Throwable th) {
        android.util.Log.i(tag,msg,th);
    }

    public static void w(String tag, String msg) {
        android.util.Log.w(tag,msg);
    }

    public static void w(String tag, String msg, Throwable th) {
        android.util.Log.w(tag,msg,th);
    }

    public static void e(String tag, String msg) {
        android.util.Log.e(tag,msg);
    }

    public static void e(String tag, String msg, Throwable th) {
        android.util.Log.e(tag,msg,th);
    }

}
