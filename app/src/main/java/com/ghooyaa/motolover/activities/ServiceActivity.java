package com.ghooyaa.motolover.activities;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.adapter.ServiceHistoryAdapter;
import com.ghooyaa.motolover.adapter.TabAdapter;
import com.ghooyaa.motolover.custom.ListviewDialog;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.fragments.ServiceHistoryFragment;
import com.ghooyaa.motolover.model.ServiceHistory;
import com.ghooyaa.motolover.utils.Utility;

import java.util.ArrayList;

public class ServiceActivity extends AppCompatActivity {

    EditText last_service_date;
    EditText service_cost;
    EditText odometer_reading;
    EditText next_service_date;

    DatabaseHelper mDatabaseHelper;

    Button save;

    Toolbar toolbar;

    EditText editText_toolbar;
    TextView textView_toolbar;
    ImageView back_toolbar;

    String v_name;
    String v_id = "0";
    String TAG = "ServiceActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);

        toolbar = (Toolbar) findViewById(R.id.toolbar_service);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabaseHelper = new DatabaseHelper(this);


        editText_toolbar = (EditText) findViewById(R.id.edit_service);
        last_service_date = (EditText) findViewById(R.id.last_service_date);
        service_cost = (EditText) findViewById(R.id.service_cost);
        odometer_reading = (EditText) findViewById(R.id.odometer_reading);
        next_service_date = (EditText) findViewById(R.id.next_service_date);
        save = (Button) findViewById(R.id.save_service);

        textView_toolbar = (TextView) findViewById(R.id.text_toolbar_service);
        final CardView cardView = (CardView) findViewById(R.id.card_service);
        final TextView textView = (TextView) findViewById(R.id.select_text_service);

        textView_toolbar.setVisibility(View.GONE);
        editText_toolbar.setVisibility(View.VISIBLE);

        last_service_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(last_service_date);
            }
        });
        next_service_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(next_service_date);
            }
        });

        editText_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListviewDialog listviewDialog = new ListviewDialog(ServiceActivity.this, new ListviewDialog.iDialog() {
                    @Override
                    public void setVehicle(String str, String vid) {
                        editText_toolbar.setText(str);
                        v_name = str;
                        v_id = vid;

                        cardView.setVisibility(View.VISIBLE);
                        save.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);
                        addLastDate(v_id);
                        Log.d(TAG, "setVehicle : " + str + "\t" + vid);
                    }
                }, v_id);
                Log.d(TAG, "current value : " + v_id);
                listviewDialog.show();
            }
        });

        if(TextUtils.equals(editText_toolbar.getText().toString(),""))
        {
            Log.d(TAG,"text is 1 : "+editText_toolbar.getText().toString());
            cardView.setVisibility(View.GONE);
            save.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }

        Intent intent = getIntent();
        if (intent.hasExtra("id")) {
            //editText_toolbar.setText(intent.getStringExtra("name"));
            v_id = intent.getStringExtra("id");
            cardView.setVisibility(View.VISIBLE);
            save.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);
            textView_toolbar.setText(intent.getStringExtra("name"));
            textView_toolbar.setVisibility(View.VISIBLE);
            editText_toolbar.setVisibility(View.GONE);
            addLastDate(v_id);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "on save clicked id = " + v_id);
                Utility utility = new Utility();
                long diff = utility.dateFormat(last_service_date.getText().toString(),next_service_date.getText().toString());

                if (last_service_date.getText().toString().equals("") || service_cost.getText().toString().equals("") || odometer_reading.getText().toString().equals("") || next_service_date.getText().toString().equals("")) {
                    Utility.alertDialogShow(ServiceActivity.this, "Alert", "Please enter all the details before add service details");
                }
                else if(diff == -1)
                {
                    utility.alertDialogShow(ServiceActivity.this, "Alert", "Enter valid refill date (must be greater than fill date) ");
                    next_service_date.setText("dd/mm/yyyy");
                }
                else {
                    mDatabaseHelper = new DatabaseHelper(getApplicationContext());
                    boolean result = mDatabaseHelper.insertService(v_id, last_service_date.getText().toString(),
                            service_cost.getText().toString(),
                            odometer_reading.getText().toString(),
                            next_service_date.getText().toString());

                    Intent mintent = new Intent();
                    mintent.putExtra("v_id", v_id);
                    setResult(3, mintent);
                    finish();
                }
            }
        });

        // Button notification = (Button) findViewById(R.id.notification);
   /*     notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NotificationCompat.Builder builder =
                        (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext())
                                .setSmallIcon(R.drawable.meter1)
                                .setContentTitle("Notifications Example")
                                .setContentText("This is a test notification");

                Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(0, builder.build());
            }

        });*/
    }

    public void addLastDate(String v_id) {
        String lastDate = null;
        Cursor cursor = mDatabaseHelper.getLastService(v_id);
        Cursor cursor1 = mDatabaseHelper.getExactVehicle(v_id);
        while (cursor.moveToNext()) {
            lastDate = cursor.getString(4);
        }
        if(TextUtils.equals(lastDate,null))
        {Log.d(TAG,"last_date:"+lastDate);
            while (cursor1.moveToNext()) {
                last_service_date.setText(cursor1.getString(7));
            }
        }
        else
        {
            Log.d(TAG,"last_date1:"+lastDate);
            last_service_date.setText(lastDate);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void getDatePicker(final EditText editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(ServiceActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

}
