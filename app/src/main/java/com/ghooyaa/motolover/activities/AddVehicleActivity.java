package com.ghooyaa.motolover.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.utils.Utility;

import java.io.ByteArrayOutputStream;
import android.Manifest.permission;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddVehicleActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText V_name;
    EditText V_Rno;

    CircleImageView vehicle_icon;

    Spinner distance_unit;
    Spinner fuel_unit;
    Spinner cost_unit;
    Spinner V_type;

    EditText last_service_date;
    //EditText next_service_date;

    String name;
    String number;
    String cost;
    String distance;
    String fuel;
    String last_date;
    String next_date;
    String image_uri;
    String type;

    String TAG = "AddVehicleActivity";

    int Image_request_code = 6;
    int PERMISSION_REQUEST_CODE = 7;

    ImageView back_toolbar;
    ImageView check_toolbar;

    String[] vehicle_type = {"bike","car"};
    String[] distance_items = {"km","mile"};
    String[] fuel_items = {"ltr","gln"};
    String[] consumption_items = {"km/ltr","mile/ltr","km/gln","mile/gln"};
    String[] cost_items = {"AUD","CAD","GBP","INR","MYR","NZD","SGD","USD"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);

        toolbar = (Toolbar) findViewById(R.id.toolbar_addVehicle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        V_name = (EditText) findViewById(R.id.vehicle_name);
        V_Rno  = (EditText) findViewById(R.id.registration_no);
        V_type = (Spinner) findViewById(R.id.vehicle_type);

        vehicle_icon = (CircleImageView) findViewById(R.id.vehicle_icon);

        distance_unit = (Spinner) findViewById(R.id.spinner_distance);
        fuel_unit = (Spinner) findViewById(R.id.spinner_fuel);
        cost_unit = (Spinner) findViewById(R.id.spinner_consumption);

        last_service_date = (EditText) findViewById(R.id.last_service_date);
        //next_service_date = (EditText) findViewById(R.id.next_service_date);

        ArrayAdapter arrayAdapter1 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,distance_items);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,fuel_items);
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,cost_items);
        ArrayAdapter arrayAdapter4 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,vehicle_type);

        distance_unit.setAdapter(arrayAdapter1);
        fuel_unit.setAdapter(arrayAdapter2);
        cost_unit.setAdapter(arrayAdapter3);
        V_type.setAdapter(arrayAdapter4);


        last_service_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(last_service_date);
            }
        });

        vehicle_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(runtomrRequestPermiccion())
                {
                    //Toast.makeText(AddVehicleActivity.this, "Permission granted", Toast.LENGTH_SHORT).show();
                    /*Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent,Image_request_code);*/
                    onPermission();
                }
                else
                {
                    //Toast.makeText(AddVehicleActivity.this, "Permission not granted", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(AddVehicleActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                    onPermission();
                }



            }
        });

        /*next_service_date.setOnClickListener(new View.OnClickListener
        () {
            @Override
            public void onClick(View view) {
                getDatePicker(next_service_date);
            }
        });*/

        distance_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                distance = distance_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        fuel_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fuel = fuel_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        cost_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cost = cost_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        V_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                image_uri = vehicle_type[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu,menu);
        return true;
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            finish();
        }
        if(id == R.id.save)
        {
            name = V_name.getText().toString();
            number = V_Rno.getText().toString();
            last_date = last_service_date.getText().toString();
            //next_date = next_service_date.getText().toString();

            if(name.equals("")||number.equals(""))
            {
                Utility.alertDialogShow(AddVehicleActivity.this,"Alert","Please enter all the details before add vehicle");
            }
            else {

                DatabaseHelper database = new DatabaseHelper(getApplicationContext());

                Bitmap image = ((BitmapDrawable)vehicle_icon.getDrawable()).getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte vehicleImage[] = stream.toByteArray();

                boolean result1 = database.insertImage(vehicleImage);
                if(result1==true)
                {
                    Toast.makeText(AddVehicleActivity.this, "Image inserted", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(AddVehicleActivity.this, "Image insertion failed", Toast.LENGTH_SHORT).show();
                }

                Log.d(TAG,"Image URI before insert : "+image_uri);
                boolean result = database.insertVehicle(name, number, image_uri, distance, fuel, cost, last_service_date.getText().toString()/*, next_service_date.getText().toString()*/);

                Intent intent = new Intent();
                intent.putExtra("name",name);
                intent.putExtra("number",number);
                intent.putExtra("last_service_date",last_service_date.getText().toString());
                setResult(1,intent);
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
    @TargetApi(Build.VERSION_CODES.N)
    private void getDatePicker(final EditText editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(AddVehicleActivity.this,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                 editText.setText(dayOfMonth + "/"+ (monthOfYear + 1) + "/" + year);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if(requestCode == Image_request_code && resultCode == RESULT_OK && null != data)
            {
                Uri URI = data.getData();
                image_uri = URI.toString();
                vehicle_icon.setImageURI(URI);
                Log.d(TAG,"Image URI is : "+image_uri);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
    }

    public boolean runtomrRequestPermiccion(){

        int result = ContextCompat.checkSelfPermission(getApplicationContext(), permission.READ_EXTERNAL_STORAGE);
        return result==PackageManager.PERMISSION_GRANTED;
    }

    public void onPermission(){
        if(runtomrRequestPermiccion()) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent,Image_request_code);
        }

    }
}
