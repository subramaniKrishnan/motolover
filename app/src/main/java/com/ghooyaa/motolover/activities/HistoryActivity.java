package com.ghooyaa.motolover.activities;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.adapter.TabAdapter;
import com.ghooyaa.motolover.custom.ListviewDialog;
import com.ghooyaa.motolover.fragments.MileageHistoryFragment;
import com.ghooyaa.motolover.fragments.ServiceHistoryFragment;

public class HistoryActivity extends AppCompatActivity {

    ImageView back_toolbar;
    EditText editText_toolbar;

    TabLayout tabLayout;
    ViewPager viewPager;

    String v_id;

    public ihistory history ;

    public interface ihistory{

        void getHistory(String vehicle_id);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        getSupportActionBar().hide();



        //editText_toolbar = (EditText) findViewById(R.id.edit_history);

        back_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager = (ViewPager) findViewById(R.id.pager);

        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragments(new MileageHistoryFragment(),"Mileage");
        adapter.addFragments(new ServiceHistoryFragment(),"Service");

        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);

        editText_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListviewDialog listviewDialog = new ListviewDialog(HistoryActivity.this, new ListviewDialog.iDialog() {
                    @Override
                    public void setVehicle(String str, String vid) {
                        editText_toolbar.setText(str);
                        v_id = vid;
                        //history.getHistory("hai");

                    }

                },v_id);
                listviewDialog.show();
            }
        });




    }

}
