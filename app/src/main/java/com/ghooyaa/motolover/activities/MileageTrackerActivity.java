package com.ghooyaa.motolover.activities;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.icu.text.DecimalFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.adapter.TabAdapter;
import com.ghooyaa.motolover.custom.ListviewDialog;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.fragments.HistoryFragment;
import com.ghooyaa.motolover.utils.Utility;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MileageTrackerActivity extends AppCompatActivity {

    EditText fill_reading;
    EditText fill_fuel_price;
    EditText fill_amount;
    EditText fill_fuel_quantity;
    EditText fill_date;

    EditText refill_reading;
    EditText refill_fuel_price;
    EditText refill_amount;
    EditText refill_fuel_quantity;
    EditText refill_date;

    TextView result_text;
    TextView result;

    Toolbar toolbar;
    EditText editText_toolbar;
    TextView textView_toolbar;

    CardView cardView1;
    CardView cardView2;
    LinearLayout cardView3;
    TextView textView;

    String v_id;

    DatabaseHelper mDatabaseHelper = null;

    public refresh irefresh;

    public interface refresh {
        void onRefresh(String vehicleID);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mileage_tracker);

        toolbar = (Toolbar) findViewById(R.id.toolbar_mileageTracker);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDatabaseHelper = new DatabaseHelper(this);

        result_text = (TextView) findViewById(R.id.result_mileage_text);
        result = (TextView) findViewById(R.id.result_mileage);
        editText_toolbar = (EditText) findViewById(R.id.edit_mileagetracker);

        fill_reading = (EditText) findViewById(R.id.fill_reading);
        fill_fuel_price = (EditText) findViewById(R.id.fill_fuel_price);
        fill_amount = (EditText) findViewById(R.id.fill_amount);
        fill_fuel_quantity = (EditText) findViewById(R.id.fill_fuel_quantity);
        fill_date = (EditText) findViewById(R.id.fill_date);
        refill_reading = (EditText) findViewById(R.id.refill_reading);
        refill_fuel_price = (EditText) findViewById(R.id.refill_fuel_price);
        refill_amount = (EditText) findViewById(R.id.refill_amount);
        refill_fuel_quantity = (EditText) findViewById(R.id.refill_fuel_quantity);
        refill_date = (EditText) findViewById(R.id.refill_date);

        textView_toolbar = (TextView) findViewById(R.id.text_toolbar_mileage);
        cardView1 = (CardView) findViewById(R.id.card_mileage1);
        cardView2 = (CardView) findViewById(R.id.card_mileage2);
        cardView3 = (LinearLayout) findViewById(R.id.card_mileage3);
        textView = (TextView) findViewById(R.id.select_text_mileage);

        textView_toolbar.setVisibility(View.GONE);
        editText_toolbar.setVisibility(View.VISIBLE);

        editText_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ListviewDialog listviewDialog = new ListviewDialog(MileageTrackerActivity.this, new ListviewDialog.iDialog() {
                    @Override
                    public void setVehicle(String str, String vid) {
                        editText_toolbar.setText(str);
                        v_id = vid;

                        cardView1.setVisibility(View.VISIBLE);
                        cardView3.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);

                        if (v_id != "0") { fillData(v_id); }

                    }
                }, v_id);
                listviewDialog.show();
            }
        });

        refill_amount.setEnabled(false);
        fill_amount.setEnabled(false);

        if(TextUtils.equals(editText_toolbar.getText().toString(),""))
        {
            Log.d("MileageTrackerActivity","text is 1 : "+editText_toolbar.getText().toString());
            cardView1.setVisibility(View.GONE);
            cardView2.setVisibility(View.GONE);
            cardView3.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }

        Intent intent = getIntent();
        if (intent.hasExtra("id")) {
            //editText_toolbar.setText(intent.getStringExtra("name"));
            v_id = intent.getStringExtra("id");
            cardView1.setVisibility(View.VISIBLE);
            cardView2.setVisibility(View.VISIBLE);
            cardView3.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);

            textView_toolbar.setText(intent.getStringExtra("name"));
            textView_toolbar.setVisibility(View.VISIBLE);
            editText_toolbar.setVisibility(View.GONE);
            fillData(v_id);
        }

        fill_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(fill_date);
            }
        });

        refill_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(refill_date);
            }
        });

        fill_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable s) {

                /*if (fill_fuel_price.getText().toString().equals("0.00")) {
                    Utility.alertDialogShow(MileageTrackerActivity.this,"Atert","Enter the fuel price");
                }
                else */if (fill_amount.getText().toString().equals("")) {
                        fill_fuel_quantity.setText("0.00");
                    }
                else {
                    String price;
                        if(TextUtils.equals(fill_fuel_price.getText().toString(),""))
                        {
                            price = "0.00";
                        }
                    else
                        {
                            price = fill_fuel_price.getText().toString();
                        }
                        float d1 = Float.parseFloat(price);
                        float d2 = Float.parseFloat(s.toString());
                        float op = d2 / d1;
                        DecimalFormat df = new DecimalFormat("#.##");
                        fill_fuel_quantity.setText("" + df.format(op));


                    }

            }
        });

        fill_fuel_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(TextUtils.equals(editable,"0.00")){
                    fill_amount.setEnabled(false);
                }
                else if(TextUtils.equals(fill_fuel_price.getText().toString(),"")){
                    fill_amount.setText("0.00");
                    fill_amount.setEnabled(false);
                    fill_fuel_quantity.setText("0.00");
                }
                else
                {
                    fill_amount.setEnabled(true);
                }

            }
        });

        fill_reading.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(fill_reading.getText().toString()==""){
                    refill_reading.setText("0");
                }
                refill_reading.setText("0");
            }
        });

        refill_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable s) {

                /*if(refill_fuel_price.getText().toString().equals("0.00"))
                {
                    Utility.alertDialogShow(MileageTrackerActivity.this,"Atert","Enter the fuel price");
                }
                else*/
                    if (refill_amount.getText().toString().equals("")) {
                        refill_fuel_quantity.setText("0.00");
                    } else {
                        String price;
                        if(TextUtils.equals(refill_fuel_price.getText().toString(),""))
                        {
                            price = "0.00";
                        }
                        else
                        {
                            price = refill_fuel_price.getText().toString();
                        }
                        float d1 = Float.parseFloat(price);
                        float d2 = Float.parseFloat(s.toString());
                        float op = d2 / d1;
                        DecimalFormat df = new DecimalFormat("#.##");
                        refill_fuel_quantity.setText("" + df.format(op));
                    }
            }
        });

        refill_reading.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable editable) {

                if (refill_reading.getText().toString().equals("")) {
                    result.setText("0");
                } else {
                    float current = Float.parseFloat(refill_reading.getText().toString());
                    float last;
                    float fuel = Float.parseFloat(fill_fuel_quantity.getText().toString());

                    if(TextUtils.equals(fill_reading.getText().toString(),""))
                    {
                        last = 0;
                    }
                    else
                    {
                        last = Float.parseFloat(fill_reading.getText().toString());
                    }
                    if ((current > last)) {
                        float diff = current - last;
                        float op = diff / fuel;
                        DecimalFormat df = new DecimalFormat("#.#");
                        result.setText("" + df.format(op));
                    } else {
                        result.setText("0");
                    }
                }
            }
        });

        refill_fuel_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                /*refill_amount.setText("0.00");
                refill_fuel_quantity.setText("0.00");*/
                if(TextUtils.equals(refill_fuel_price.getText().toString(),"0.00")){
                    refill_amount.setEnabled(false);
                }
                else if(TextUtils.equals(refill_fuel_price.getText().toString(),"")){
                    refill_amount.setText("0.00");
                    refill_amount.setEnabled(false);
                    refill_fuel_quantity.setText("0.00");
                }
                else
                {
                    refill_amount.setEnabled(true);
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        if (id == R.id.save) {

            if (textView.getVisibility() == View.VISIBLE) {

                Utility.alertDialogShow(MileageTrackerActivity.this,"Alert","Before save select a vehicle and input details");

            } else {

                String mileage_unit = null;
                String distance_unit = null;
                String cost_unit = null;

                Utility utility = new Utility();

                Cursor cursor = mDatabaseHelper.getExactVehicle(v_id);
                while (cursor.moveToNext()) {

                    mileage_unit = cursor.getString(4);
                    distance_unit = cursor.getString(5);
                    cost_unit = cursor.getString(6);
                }

                int distance;
                int current;
                int last;
                long No_of_days;
                String cost;
                String mileage_result;

                current = Integer.parseInt(refill_reading.getText().toString());
                last = Integer.parseInt(fill_reading.getText().toString());

                distance = current - last;
                cost = fill_amount.getText().toString();
                mileage_result = result.getText().toString();
                No_of_days = utility.dateFormat(fill_date.getText().toString(), refill_date.getText().toString());

                Log.d("save", "mileage tracker id = " + v_id);
                if (fill_reading.getText().toString().equals("0") || fill_fuel_price.getText().toString().equals("0.00") || fill_amount.getText().toString().equals("0.00") || fill_fuel_quantity.getText().toString().equals("0.00") || fill_date.getText().toString().equals("dd/mm/yyyy")) {
                    utility.alertDialogShow(MileageTrackerActivity.this, "Alert", "Please enter correct details");
                } else if (!(refill_date.getText().toString().equals("dd/mm/yyyy")) && No_of_days == -1) {
                    utility.alertDialogShow(MileageTrackerActivity.this, "Alert", "Enter valid refill date (must be greater than fill date) ");
                    refill_date.setText("dd/mm/yyyy");
                } else {
                    boolean result1 = mDatabaseHelper.insertFill(v_id, fill_reading.getText().toString(),
                            fill_fuel_price.getText().toString(),
                            fill_amount.getText().toString(),
                            fill_fuel_quantity.getText().toString(),
                            fill_date.getText().toString());

                    boolean result2 = mDatabaseHelper.insertReFill(v_id, refill_reading.getText().toString(),
                            refill_fuel_price.getText().toString(),
                            refill_amount.getText().toString(),
                            refill_fuel_quantity.getText().toString(),
                            refill_date.getText().toString());

                    if (!(refill_date.getText().toString().equals("dd/mm/yyyy"))) {
                        boolean result3 = mDatabaseHelper.insertMileage(v_id, mileage_result, distance, cost, No_of_days, mileage_unit, distance_unit, cost_unit);

                        if (result3 == true) {
                            Toast.makeText(MileageTrackerActivity.this, "data inserted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MileageTrackerActivity.this, "data not inserted", Toast.LENGTH_SHORT).show();
                        }
                        Intent intent = new Intent();
                        intent.putExtra("v_id", v_id);
                        setResult(2, intent);
                    }
                    finish();
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void fillData(String vehicleID) {

        String f_reading = null;
        String f_fp = null;
        String f_amount = null;
        String f_fq = null;
        String f_date = null;

        String rf_reading = null;
        String rf_fp = null;
        String rf_amount = null;
        String rf_fq = null;
        String rf_date = null;

      //  Toast.makeText(MileageTrackerActivity.this, "id : " + vehicleID, Toast.LENGTH_SHORT).show();
        Cursor cursor = mDatabaseHelper.getLastReFill(vehicleID);
        while (cursor.moveToNext()) {
            rf_reading = cursor.getString(1);
            rf_fp = cursor.getString(2);
            rf_amount = cursor.getString(3);
            rf_fq = cursor.getString(4);
            rf_date = cursor.getString(5);

        }
        Cursor cursor1 = mDatabaseHelper.getLastFill(vehicleID);
        while (cursor1.moveToNext()) {
            f_reading = cursor1.getString(1);
            f_fp = cursor1.getString(2);
            f_amount = cursor1.getString(3);
            f_fq = cursor1.getString(4);
            f_date = cursor1.getString(5);
        }

        //TextUtils.equals(rf_reading,"0");

        if (TextUtils.equals(rf_reading, "0")) {
            Log.d("MileageTracker", "rf_reading1 : " + rf_reading);
            fill_reading.setText(f_reading);
        } else if (TextUtils.equals(rf_reading, null)) {
            Log.d("MileageTracker", "rf_reading2 : " + rf_reading);
            fill_reading.setText("0");
        } else {
            Log.d("MileageTracker", "rf_reading3 : " + rf_reading);
            fill_reading.setText(rf_reading);
        }

        if (TextUtils.equals(rf_fp, "0.00")) {
            Log.d("MileageTracker", "rf_fp1 : " + rf_fp);
            fill_fuel_price.setText(f_fp);
        } else if (TextUtils.equals(rf_fp, null)) {
            Log.d("MileageTracker", "rf_fp2 : " + rf_fp);
            fill_fuel_price.setText("0.00");
        } else {
            Log.d("MileageTracker", "rf_fp3 : " + rf_fp);
            fill_fuel_price.setText(rf_fp);
        }

        if (TextUtils.equals(rf_amount, "0.00")) {
            fill_amount.setText(f_amount);
        } else if (TextUtils.equals(f_amount, null)) {
            fill_amount.setText("0.00");
        } else {
            fill_amount.setText(rf_amount);
        }

        if (TextUtils.equals(rf_fq, "0.00")) {
            fill_fuel_quantity.setText(f_fq);
        } else if (TextUtils.equals(f_fq, null)) {
            fill_fuel_quantity.setText("0.00");
        } else {
            fill_fuel_quantity.setText(rf_fq);
        }

        if (TextUtils.equals(rf_date, "dd/mm/yyyy")) {
            fill_date.setText(f_date);
        } else if (TextUtils.equals(f_date, null)) {
            fill_date.setText("dd/mm/yyyy");
        } else {
            fill_date.setText(rf_date);
        }


        if(TextUtils.equals(fill_reading.getText().toString(),"0") || TextUtils.equals(fill_fuel_price.getText().toString(),"0.00")){
            cardView2.setVisibility(View.GONE);
            result.setVisibility(View.GONE);
            result_text.setVisibility(View.GONE);
        }
        else {
            cardView2.setVisibility(View.VISIBLE);
            result.setVisibility(View.VISIBLE);
            result_text.setVisibility(View.VISIBLE);
            fill_reading.setEnabled(false);
            fill_fuel_price.setEnabled(false);
            fill_amount.setEnabled(false);
            fill_fuel_quantity.setEnabled(false);
            fill_date.setEnabled(false);
        }

    }

    @TargetApi(Build.VERSION_CODES.N)
    private void getDatePicker(final EditText editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(MileageTrackerActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


}
