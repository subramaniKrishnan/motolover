package com.ghooyaa.motolover.activities;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.AlteredCharSequence;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.utils.Utility;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManageActivity extends AppCompatActivity {

    Toolbar toolbar;

    CircleImageView Vicon;
    EditText Vname;
    EditText Vno;
    EditText last_service_date;
    EditText next_service_date;

    Spinner distance_unit;
    Spinner fuel_unit;
    Spinner cost_unit;
    Spinner V_type;


    int Image_Reguest = 5;
    String image_uri;
    String v_id;
    String name;
    String number;
    String cost;
    String distance;
    String fuel;
    String last_date;
    String next_date;
    String type;

    String[] vehicle_type = {"bike","car"};
    String[] distance_items = {"km","mile"};
    String[] fuel_items = {"ltr","gln"};
    String[] consumption_items = {"km/ltr","mile/ltr","km/gln","mile/gln"};
    String[] cost_items = {"AUD","CAD","GBP","INR","MYR","NZD","SGD","USD"};

    DatabaseHelper mDatabaseHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);

        toolbar = (Toolbar) findViewById(R.id.toolbar_manage);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabaseHelper = new DatabaseHelper(this);

        Vicon = (CircleImageView) findViewById(R.id.vImage_update);

        Vname = (EditText) findViewById(R.id.vehicel_name_update);
        Vno = (EditText) findViewById(R.id.vehicle_no_update);
        last_service_date = (EditText) findViewById(R.id.last_service_date_update);
       // next_service_date = (EditText) findViewById(R.id.vehicel_name_update);

        V_type = (Spinner) findViewById(R.id.spinner_vehicle_update);
        distance_unit = (Spinner) findViewById(R.id.spinner_distance_update);
        fuel_unit = (Spinner) findViewById(R.id.spinner_fuel_update);
        cost_unit = (Spinner) findViewById(R.id.spinner_consumption_update);


        ArrayAdapter arrayAdapter1 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,distance_items);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,fuel_items);
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,cost_items);
        ArrayAdapter arrayAdapter4 = new ArrayAdapter(this,R.layout.res_spinner,R.id.spinner_text,vehicle_type);

        distance_unit.setAdapter(arrayAdapter1);
        fuel_unit.setAdapter(arrayAdapter2);
        cost_unit.setAdapter(arrayAdapter3);
        V_type.setAdapter(arrayAdapter4);

        last_service_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDatePicker(last_service_date);
            }
        });

        Vicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, Image_Reguest);
            }
        });

        Intent intent = getIntent();
        if(intent.hasExtra("ID"))
        {
            v_id = intent.getStringExtra("ID");
            Cursor cursor = mDatabaseHelper.getExactVehicle(v_id);
            while(cursor.moveToNext()){

                Vname.setText(cursor.getString(1));
                Vno.setText(cursor.getString(2));
                type = cursor.getString(3);
                distance = cursor.getString(4);
                fuel = cursor.getString(5);
                cost = cursor.getString(6);
                last_service_date.setText(cursor.getString(7));
                if(TextUtils.equals(type,"bike"))
                {
                    Vicon.setImageResource(R.drawable.bike);
                }
                else if(TextUtils.equals(type,"car"))
                {
                    Vicon.setImageResource(R.drawable.car);
                }
                else
                {
                    Vicon.setImageURI(Uri.parse(type));
                }


                Log.d("ManageActivity","Values : "+type+"\n"+distance+"\n"+fuel+"\n"+ cost);

                setVelues();

            }
        }

        distance_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                distance = distance_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        fuel_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fuel = fuel_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        cost_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cost = cost_items[i];
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
       /* V_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                type = vehicle_type[i];
                if(type=="bike"){
                    Vicon.setImageResource(R.drawable.bike);
                }
                else
                {
                    Vicon.setImageResource(R.drawable.car);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.update_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                finish();
                break;

            case R.id.delete:
                final AlertDialog.Builder alert = new AlertDialog.Builder(ManageActivity.this);
                alert.setMessage("Do you want to delete this vehicle");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mDatabaseHelper.deleteVehicle(v_id);
                        mDatabaseHelper.deleteFill(v_id);
                        mDatabaseHelper.deleteReFill(v_id);
                        mDatabaseHelper.deleteService(v_id);
                        mDatabaseHelper.deleteMileage(v_id);

                        Intent intent1 = new Intent();
                        setResult(1,intent1);
                        finish();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        alert.create().dismiss();
                    }
                });

                alert.show();

                break;

            case R.id.edit:
                name = Vname.getText().toString();
                number = Vno.getText().toString();
                last_date = last_service_date.getText().toString();
                //next_date = next_service_date.getText().toString();

                if(name.equals("")||number.equals(""))
                {
                    Utility.alertDialogShow(ManageActivity.this,"Alert","Please enter all the details before add vehicle");
                }
                else {
                    if(TextUtils.equals(image_uri,null)){

                        Cursor cursor= mDatabaseHelper.getExactVehicle(v_id);
                    while(cursor.moveToNext()){
                            image_uri = cursor.getString(3);
                        }
                    }
                    boolean result = mDatabaseHelper.updateVehicle(v_id,name, number, image_uri, distance, fuel, cost, last_date);
                    boolean result1 = mDatabaseHelper.updateMileage(v_id,distance,fuel,cost);
                   /* if(result1==true)
                    {
                        Toast.makeText(ManageActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(ManageActivity.this, "Updated failed", Toast.LENGTH_SHORT).show();
                    }*/
                    Intent intent = new Intent();
                    intent.putExtra("name",name);
                    intent.putExtra("number",number);
                    intent.putExtra("last_service_date",last_date);
                    setResult(1,intent);
                    finish();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setVelues() {
        if(TextUtils.equals(type,"car"))  {
            V_type.setSelection(1);
        }
        else
        {
            V_type.setSelection(0);
        }
        if(TextUtils.equals(distance,"km"))
        {
            distance_unit.setSelection(0);
        }
        else
        {
            distance_unit.setSelection(1);
        }
        if(TextUtils.equals(fuel,"ltr"))
        {
            fuel_unit.setSelection(0);
        }
        else
        {
            fuel_unit.setSelection(1);
        }

        switch (cost){
            case "AUD":
                cost_unit.setSelection(0);
                break;
            case "CAD":
                cost_unit.setSelection(1);
                break;
            case "GBP":
                cost_unit.setSelection(2);
                break;
            case "INR":
                cost_unit.setSelection(3);
                break;
            case "MYR":
                cost_unit.setSelection(4);
                break;
            case "NZD":
                cost_unit.setSelection(5);
                break;
            case "SGD":
                cost_unit.setSelection(6);
                break;
            case "USD":
                cost_unit.setSelection(7);
                break;
        }

        /*if(TextUtils.equals(cost,"km/ltr"))
        {
            cost_unit.setSelection(0);
        }
        else if(TextUtils.equals(cost,"mile/ltr"))
        {
            cost_unit.setSelection(1);
        }
        else if(TextUtils.equals(cost,"km/gln"))
        {
            cost_unit.setSelection(2);
        }
        else
        {
            cost_unit.setSelection(3);
        }*/
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void getDatePicker(final EditText editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(ManageActivity.this,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                // Display Selected date in textbox
                editText.setText(dayOfMonth + "/"+ (monthOfYear + 1) + "/" + year);

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {

            if (requestCode == Image_Reguest && resultCode == RESULT_OK
                    && null != data) {
                Uri URI = data.getData();
                Vicon.setImageURI(URI);
                image_uri = URI.toString();
                Log.d("ManageActivity","Image URI is : "+URI);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
    }
}
