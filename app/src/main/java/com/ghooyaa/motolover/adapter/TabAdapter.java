package com.ghooyaa.motolover.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.ghooyaa.motolover.fragments.MileageHistoryFragment;
import com.ghooyaa.motolover.fragments.ServiceHistoryFragment;

import java.util.ArrayList;

/**
 * Created by nekhop on 8/8/2017.
 */
public class TabAdapter extends FragmentPagerAdapter {

   // public VehicleID vehicleID;
    ArrayList<Fragment> fragment = new ArrayList<>();
    ArrayList<String> title = new ArrayList<>();
    String vid;
    private MileageHistoryFragment mMileageHistoryFragment;
    private ServiceHistoryFragment mServiceHistoryFragment;

    public TabAdapter(FragmentManager fm) {
        super(fm);
        title.add("Mileage");
        title.add("Service");
    }

    @Override
    public Fragment getItem(int position) {

        Log.d("TabAdapter","getItem : "+position);

        switch (position) {
            case 0:
                mMileageHistoryFragment = new MileageHistoryFragment();
                return mMileageHistoryFragment;
            case 1:
                mServiceHistoryFragment = new ServiceHistoryFragment();
                return mServiceHistoryFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        /*return super.getPageTitle(position);*/
        return title.get(position);
    }

    public void addFragments(Fragment fragment, String title) {
        this.fragment.add(fragment);
        this.title.add(title);
    }


    /*public void doRefresh(String vehicleId) {


           if(mMileageHistoryFragment == null && mServiceHistoryFragment == null)
           {
               Log.d("TabAdapter", "doRefresh : " + vehicleId);
               Log.d("TabAdapter", "object null " );
               mMileageHistoryFragment = new MileageHistoryFragment();
               mServiceHistoryFragment = new ServiceHistoryFragment();
               mServiceHistoryFragment.onRefreshFilter(vehicleId);
               mMileageHistoryFragment.onRefreshFilter(vehicleId);
               Log.d("TabAdapter", "object created " );
           }else{
               Log.d("TabAdapter", "object created " );
               Log.d("refresh","TabAdapter id : "+vehicleId);
               mServiceHistoryFragment.onRefreshFilter(vehicleId);
               mMileageHistoryFragment.onRefreshFilter(vehicleId);
           }
    }*/

    public void doRefresh(String vehicleId, String vehicleName) {


        if(mMileageHistoryFragment == null && mServiceHistoryFragment == null)
        {
            Log.d("TabAdapter", "doRefresh : " + vehicleId);
            Log.d("TabAdapter", "object null " );
            mMileageHistoryFragment = new MileageHistoryFragment();
            mServiceHistoryFragment = new ServiceHistoryFragment();
            mServiceHistoryFragment.onRefreshFilter(vehicleId,vehicleName);
            mMileageHistoryFragment.onRefreshFilter(vehicleId,vehicleName);
            Log.d("TabAdapter", "object created " );
        }else{
            Log.d("TabAdapter", "object created " );
            Log.d("refresh","TabAdapter id : "+vehicleId);
            mServiceHistoryFragment.onRefreshFilter(vehicleId,vehicleName);
            mMileageHistoryFragment.onRefreshFilter(vehicleId,vehicleName);
        }
    }
}
