package com.ghooyaa.motolover.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.model.MileageHistory;

import java.util.ArrayList;

/**
 * Created by nekhop on 8/8/2017.
 */
public class MileageHistoryAdapter extends RecyclerView.Adapter<MileageHistoryAdapter.ViewHolder> {

    ArrayList<MileageHistory> mileageHistory = new ArrayList<>();
    Context context;

    public MileageHistoryAdapter(ArrayList<MileageHistory> mileageHistory, Context context) {
        this.mileageHistory = mileageHistory;
        this.context = context;
    }

    public MileageHistoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MileageHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.res_mileage_history,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MileageHistoryAdapter.ViewHolder holder, int position) {

        holder.mileage.setText(this.mileageHistory.get(position).getMileage());
        holder.distance.setText(this.mileageHistory.get(position).getDistance());
        holder.cost.setText(this.mileageHistory.get(position).getCost());
        holder.no_of_days.setText(this.mileageHistory.get(position).getNo_of_days());

        holder.mileage_unit.setText(this.mileageHistory.get(position).getMileage_unit());
        holder.distance_unit.setText(this.mileageHistory.get(position).getDistance_unit());
        holder.cost_unit.setText(this.mileageHistory.get(position).getCost_unit());


    }

    @Override
    public int getItemCount() {
        return mileageHistory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mileage;
        TextView distance;
        TextView cost;
        TextView no_of_days;

        TextView mileage_unit;
        TextView distance_unit;
        TextView cost_unit;

        public ViewHolder(View itemView) {
            super(itemView);

            mileage = itemView.findViewById(R.id.mileage);
            distance = itemView.findViewById(R.id.distance);
            cost = itemView.findViewById(R.id.cost);
            no_of_days = itemView.findViewById(R.id.no_of_days);

            mileage_unit = itemView.findViewById(R.id.mileage_unit);
            distance_unit = itemView.findViewById(R.id.distance_unit);
            cost_unit = itemView.findViewById(R.id.cost_unit);



        }
    }

    public void mileageFilter(String vid){

        Log.d("tabadapter","Mileage adapter = "+vid);
        ArrayList<MileageHistory> list = new ArrayList<MileageHistory>();

        DatabaseHelper database = new DatabaseHelper(context);
        list = database.getMileage(vid);
        mileageHistory.addAll(list);
        notifyDataSetChanged();
        Log.d("tabadapter","list is : "+mileageHistory);
    }
}
