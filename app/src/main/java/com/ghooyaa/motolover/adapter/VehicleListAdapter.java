package com.ghooyaa.motolover.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.activities.AddVehicleActivity;
import com.ghooyaa.motolover.activities.ManageActivity;
import com.ghooyaa.motolover.database.DatabaseHelper;
import com.ghooyaa.motolover.fragments.HistoryFragment;
import com.ghooyaa.motolover.fragments.VehicleListFragment;
import com.ghooyaa.motolover.model.VehicleList;

import java.util.ArrayList;

/**
 * Created by nekhop on 8/8/2017.
 */
public class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.ViewHolder> {
    ArrayList<VehicleList> vehicleList = new ArrayList<>();
    Context context;

    public onResult result;

    public interface onResult{
        void onActivityRes(String id);
    }

    public VehicleListAdapter(ArrayList<VehicleList> vehicleList, Context context, onResult result) {
        this.vehicleList = vehicleList;
        this.context = context;
        this.result = result;
    }




    @Override
    public VehicleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.res_vehiclelist,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VehicleListAdapter.ViewHolder holder, final int position) {

        if(TextUtils.equals(this.vehicleList.get(position).getVehicle_image().toString(),"bike"))
        {
            holder.vehicle_image.setImageResource(R.drawable.bike);
        }
        else if(this.vehicleList.get(position).getVehicle_image().toString()=="car")
        {
            holder.vehicle_image.setImageResource(R.drawable.car);
        }
        else
        {
            holder.vehicle_image.setImageURI(Uri.parse(this.vehicleList.get(position).getVehicle_image()));
        }
       // holder.vehicle_image.setImageURI(Uri.parse(this.vehicleList.get(position).getVehicle_image()));
        holder.vehicle_name.setText(this.vehicleList.get(position).getVehicle_name());
        holder.vehicle_no.setText(this.vehicleList.get(position).getVehicle_no());
        holder.service_date.setText(this.vehicleList.get(position).getService_date());
        holder.vehicle_id.setText(this.vehicleList.get(position).getVehicle_id());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(result!=null){
                    result.onActivityRes(holder.vehicle_id.getText().toString());
                }

            }
        });

    }

     @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView vehicle_image;
        TextView vehicle_id;
        TextView vehicle_name;
        TextView vehicle_no;
        TextView service_date;

        public ViewHolder(View itemView) {
            super(itemView);

            vehicle_image = itemView.findViewById(R.id.vehicle_image);
            vehicle_id = itemView.findViewById(R.id.vehicle_id);
            vehicle_name  = itemView.findViewById(R.id.vehicle_name);
            vehicle_no  = itemView.findViewById(R.id.vehicle_no);
            service_date  = itemView.findViewById(R.id.service_date);


        }
    }


}
