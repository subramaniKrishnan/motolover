package com.ghooyaa.motolover.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.model.ListItems;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nekhop on 8/3/2017.
 */
public class ListViewAdapter extends BaseAdapter {

    ArrayList<ListItems> listitems;
    Context context;

    public ListViewAdapter(ArrayList<ListItems> item, Context context) {
        this.listitems = item;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.listitems.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void add(ListItems item) {

        this.listitems.add(0,item);
        notifyDataSetChanged();
    }

    public void add(ArrayList<ListItems> item) {
        this.add(item);
        notifyDataSetChanged();
    }


    public class Holder{

        CircleImageView vehicle_image;
        ImageView check_icon;
        TextView vehicle_name;
        TextView vehicle_Rno;
        TextView vehicle_id;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        final Holder holder = new Holder();

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.res_listview,parent,false);

        holder.vehicle_image = view.findViewById(R.id.list_image);
        holder.vehicle_name  = view.findViewById(R.id.list_name);
        holder.vehicle_Rno   = view.findViewById(R.id.list_Rno);
        holder.check_icon    = view.findViewById(R.id.list_check);
        holder.vehicle_id    = view.findViewById(R.id.list_id);

        if(TextUtils.equals(this.listitems.get(position).getVehicle_image(),"bike"))
        {
            holder.vehicle_image.setImageResource(R.drawable.bike);
        }
        else if(TextUtils.equals(this.listitems.get(position).getVehicle_image(),"car"))
        {
            holder.vehicle_image.setImageResource(R.drawable.car);
        }
        else
        {
            holder.vehicle_image.setImageURI(Uri.parse(this.listitems.get(position).getVehicle_image()));
        }
       // holder.vehicle_image.setImageURI(Uri.parse(this.listitems.get(position).getVehicle_image()));
        holder.vehicle_name.setText(this.listitems.get(position).getVehicle_name());
        holder.vehicle_Rno.setText(this.listitems.get(position).getVehicle_Rno());

        holder.vehicle_id.setText(this.listitems.get(position).getVehicle_id());

        Log.d("listviewAdapter","image status : "+this.listitems.get(position).getCheck_image());
        if(this.listitems.get(position).getCheck_image()==1) {
            holder.check_icon.setVisibility(View.VISIBLE);

        }
        return view;
    }
}
