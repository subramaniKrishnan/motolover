package com.ghooyaa.motolover.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.model.MileageHistory;
import com.ghooyaa.motolover.model.ServiceHistory;

import java.util.ArrayList;

/**
 * Created by nekhop on 8/3/2017.
 */
public class ServiceHistoryAdapter extends RecyclerView.Adapter<ServiceHistoryAdapter.ViewHolder> {

    ArrayList<ServiceHistory> serviceHistory = new ArrayList<>();
    Context context;

    public ServiceHistoryAdapter(ArrayList<ServiceHistory> serviceList, Context context) {
        this.serviceHistory = serviceList;
        this.context = context;
    }


    @Override
    public ServiceHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.res_service_history,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceHistoryAdapter.ViewHolder holder, int position) {

        holder.service_last.setText(this.serviceHistory.get(position).getService_last());
        holder.service_odometer.setText(this.serviceHistory.get(position).getService_odometer());
        holder.service_amount.setText(this.serviceHistory.get(position).getService_amount());

    }

    @Override
    public int getItemCount() {
        return serviceHistory.size();
    }

     public class ViewHolder extends RecyclerView.ViewHolder {

        TextView service_last;
        TextView service_odometer;
        TextView service_amount;

        public ViewHolder(View itemView) {
            super(itemView);

            service_last = itemView.findViewById(R.id.service_last);
            service_odometer = itemView.findViewById(R.id.service_odometer);
            service_amount = itemView.findViewById(R.id.service_amount);

        }
    }
}
