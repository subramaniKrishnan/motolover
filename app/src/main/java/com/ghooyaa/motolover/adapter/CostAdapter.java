package com.ghooyaa.motolover.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ghooyaa.motolover.R;
import com.ghooyaa.motolover.model.CostHistory;

import java.util.ArrayList;

/**
 * Created by nekhop on 9/2/2017.
 */
public class CostAdapter extends RecyclerView.Adapter<CostAdapter.ViewHolder> {

    ArrayList<CostHistory> mCostHistories = new ArrayList<>();
    Context context;

    public CostAdapter(Context context, ArrayList<CostHistory> costHistories) {

        this.context = context;
        this.mCostHistories = costHistories;

    }

    @Override
    public CostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.res_cost,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CostAdapter.ViewHolder holder, int position) {

        holder.month.setText(this.mCostHistories.get(position).getMonth());
        holder.no_of_times_filled.setText(this.mCostHistories.get(position).getNo_of_times_filled());
        holder.total_amount.setText(this.mCostHistories.get(position).getTotal_amount());
        holder.currency_unit.setText(this.mCostHistories.get(position).getCurrency());
    }

    @Override
    public int getItemCount() {
        return mCostHistories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView month;
        TextView no_of_times_filled;
        TextView total_amount;
        TextView currency_unit;

        public ViewHolder(View itemView) {
            super(itemView);

            month = itemView.findViewById(R.id.month);
            no_of_times_filled = itemView.findViewById(R.id.no_of_time_filled);
            total_amount = itemView.findViewById(R.id.total_amount);
            currency_unit = itemView.findViewById(R.id.currency);

        }
    }
}
